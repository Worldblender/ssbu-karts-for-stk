# Kart Class Tier List
![Kart class tier list](kart-class-tier-list.jpg)

**Characters within a class race exactly the same. No special abilities or items are given for particular characters.**

Characters are sorted in this order:

1. Weight, according to [SmashWiki](https://www.ssbwiki.com/Weight#Super_Smash_Bros._Ultimate_weight_values).

2. Order of costumes, if more than one appears for a given character.

3. If multiple characters have the same weight, by first appearance in Smash Bros.

By default, heavy and medium karts use the large engine sound, while light karts use the small engine sound. Exceptions follow below (alphabetical ordering):

* Heavy, but small engine sound: Bowser Jr., Hero, Yoshi

* Medium, but small engine sound: Diddy Kong, Lucina, Marth

* Light, but large engine sound: Bayonetta, Mewtwo, Rosalina & Luma, Sephiroth

## Difference between kart classes (from SuperTuxKart help):
* Mass (main classification) - there are three classes of karts, depending on their mass: light, medium and heavy. Heavier karts are less affected by parachutes and are more resistant to explosions.

* Acceleration - especially useful at start, after an accident, or in tracks with a lot of sharp curves. The lighter the kart, the faster it accelerates, especially at low speeds.

* Max speed - the higher it is, the faster the kart can go. Especially useful in tracks with straight lines and gentle curves. Heavier karts have a higher top speed.

* Nitro consumption - the lower it is, the more speed you can get from a can of nitro. The lighter the kart, the lower its nitro consumption.

## Tier list with more alternate costumes

![Kart class tier list](kart-class-tier-list-alt.jpg)

In order to keep the main list clean, only the first costume of every fighter (except for R.O.B., who uses the NES version instead of the Famicom version) over there. Here, most other alternate costumes that are a gender swap or model swap are listed here. Not every costume that can be turned into a kart appears in this list, and every costume that is a palette swap can be applied onto their base karts instead by texture replacement. The same rules apply as with the main list.

## Alternate tier list locations

### Main (contains only the first costume of every fighter):
* Ultimate Smash Tier List: <https://www.smashtierlist.com/6f497ec59547c9d38cf177979c6d57f0f385434b44c5cf8855632c284508ce01/>

* Imgur: <https://i.imgur.com/Apb2B5k.jpg>

### Alternate (some alternate costumes included):
* Ultimate Smash Tier List: <https://www.smashtierlist.com/c3b2ce3315ed76db879d31578e3a6a0e859475bca8f39e9805178c79681dc2c7/>

* Imgur: <https://i.imgur.com/MdChMRG.jpg>
