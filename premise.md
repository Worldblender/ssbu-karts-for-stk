# Premise: How the SSBU Fighters got into the Mascot Kingdom

## Characters who have never appeared in a racing game before
* Bayonetta
* Chrom
* Corrin (Kamui)
* Dark Pit
* Dark Samus
* Duck Hunt
* Falco
* Fox
* Ganondorf
* Greninja (Gekkouga)
* Ice Climbers, with both Popo and Nana
* Ike
* Incineroar (Gaogaen)
* Ken
* King K. Rool
* Little Mac
* Lucario
* Lucas
* Lucina
* Marth
* Mewtwo
* Mr. Game & Watch
* Ness
* Olimar
* Palutena
* Pit
* The Pokémon Trainer's Pokémon
* Richter
* Ridley
* Robin (Reflet)
* Roy
* Ryu
* Samus
* Sheik
* Shulk
* Simon
* Snake
* Toon Link
* Wii Fit Trainer
* Wolf
* Young Link
* Zelda
* Zero Suit Samus

## A Super Smash Bros. + SuperTuxKart crossover
*Part of this story is based from <https://www.ssbwiki.com/Adventure_Mode:_World_of_Light#Plot>. The original plot from Adventure_Mode: World of Light plays, right up to what follows below.*

Kirby is seen fleeing from Galeem's beams of light with his Warp Star. He pushes the Warp Star to its limits, but unfortunately he got tired, running out of energy of drive it further. The Warp Star disintegrates, and Kirby got caught by one of Galeem's light beams after that. With no more fighters left to catch, the light beams are then shown vaporizing some non-playable characters (that will never appear again from this point on). Galeem's light engulfed and vaporized  galaxies and planets, and then the entire universe (in which the Smash fighters' worlds exist in). Did everything really get consumed?
![World map of the Mascot Kingdom](https://supertuxkart.net/images/9/9c/Stkflag_worldMap_a.png)
Not really, as there exist other universes that, only in certain situations, can cross-over with each other. The scene cuts to a view in the Mascot Kingdom, inside another universe where mascots of free/open source software live. The scene shows Gnu (mascot of the GNU Project) playing a flute. *This scene plays the same as depicted in STK, up to the point where Tux came out of his mushroom house, and after Nolok leaves in his spaceship.* Tux has been left worried, for that the absence of Gnu meant that the Mascot Kingdom has become an unsafe place, and it would only get worse very soon. Tux knew and his friends saw it, the situation was about to become so: a vortex in the sky opened up, and instead of sucking up things, it spat out things instead.

Galeem and Dharkon have turned the Mascot Kingdom into their dumping grounds by warping the Smash fighters to there, instead of imprisoning them right away. Both beings ran out of worlds to destroy in their original universe, so that's why they set their sights on the Mascot Kingdom. Many at a time, all of these fighters fell out of the vortex:

* Bayonetta
* Bowser (Koopa)
* Bowser Jr. (Koopa Jr.) (the Kooplings as alt costumes)
* Captain Falcon
* Chrom
* Cloud
* Corrin (Kamui), both genders co-existing
* Daisy
* Dark Pit
* Dark Samus
* Diddy Kong
* Donkey Kong
* Dr. Mario
* Duck Hunt
* Falco
* Fox
* Ganondorf
* Greninja (Gekkouga)
* Ice Climbers, with both Popo and Nana
* Ike
* Incineroar (Gaogaen)
* Inkling, both genders co-existing
* Isabelle (Shizue)
* Jigglypuff (Purin)
* Ken
* King Dedede
* King K. Rool
* Kirby
* Link
* Little Mac
* Lucario
* Lucas
* Lucina
* Luigi
* Mario
* Marth
* Mega Man
* Meta Knight
* Mewtwo
* Mr. Game & Watch
* Ness
* Olimar (Alph as an alt costume), with a modest group of Pikmin
* Pac-Man
* Palutena
* Peach
* Pichu
* Pikachu
* Pit
* Pokémon Trainer (includes Squirtle, Ivysaur, and Charizard in separate directories)
* Richter
* Ridley
* R.O.B. (Robot)
* Robin (Reflet), both genders co-existing
* Rosalina (Rosetta)
* Roy
* Ryu
* Samus
* Sheik
* Shulk
* Simon
* Snake
* Sonic
* Toon Link
* Villager (Murabito), both genders co-existing
* Wario
* Wii Fit Trainer, both genders co-existing
* Wolf
* Young Link
* Yoshi
* Zelda
* Zero Suit Samus

Where did the fighters land? Somewhere in the large Alpalya Mountains, more specifically in the area where the Green Valley track takes place.
And they landed near Sara the Wizard's (empress of the Mascot Kingdom, and mascot of OpenGameArt, from where the base kart and a few textures originate from) palace. Tux and his friends met up with Sara just in time to see the Smash fighters falling to the ground. To save them from injuries, trampolines have been set up, and luckily, all the Smash fighters survived without major injuries.

Back at the planet where Nolok lives, he doesn't notice the presence of Dharkon. The same goes for the now-imprisoned Gnu, who doesn't notice that Galeem is also there. Suddenly, they used their powers to take over the two respective creatures, with Nolok becoming a puppet of Dharkon, and Gnu becoming a puppet for Galeem. They no longer need the Smash fighters now! Just having them two is enough for their evil plans.

To succeed in defeating Galeem and Dharkon, Tux, his friends, and the Smash fighters need to use vehicles to get there. Unfortunately, while Tux and his friends have their own karts, the Smash fighters do not. Their bodies and weapons alone aren't enough this time, and not all of them know how to drive automobiles, or never drove one before.
With the help of Mandarava Motors, a well-known automobile manufacturer, they received karts as well. To speed up this help, they all received the same kart model, although their body weights influenced how the karts will drive. After hours of talk amongest the large group, they reached agreement, and set out in their quest to save the world with kart racing.

*To be continued...*
