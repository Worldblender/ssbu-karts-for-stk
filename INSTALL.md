## Prerequisites
**SuperTuxKart 0.9.3 or later is required to use this add-on kart collection, but 1.0.0 or later is recommended.** The addons are shipped in SPM (Space-Partitioned Mesh) format (this format is unique to this game, not having appeared anywhere else yet), which only said versions of STK can read. If STK isn't already installed, download it from <https://supertuxkart.net/Download>, and follow the specific instructions for your operating system. If using a Linux/BSD distro, there should be a package for the most recent STK release, or a slightly older one; install it from there.

## How to download
Use the `git` command-line program or a Git frontend to clone this repository. Git LFS must be installed in order to successfully download all of the audiovisual files. This downloads everything, including the STK kart files and the source Blender files.

```bash
git clone https://gitlab.com/Worldblender/ssbu-karts-for-stk/
```
Alternatively, this repository can be downloaded as a compressed archive, but it will not receive updates when they are released.

## How to use/install
Copy or symlink the contents of the 'karts/' directory to one of these paths, depending on your operating system:

* On Windows: `%APPDATA%/supertuxkart/addons/karts`

* On Linux distros/BSDs: `~/.local/share/supertuxkart/addons/karts`

* On macOS: `~/Library/Application Support/supertuxkart/addons/karts`

* On Android: `<location of internal storage>/stk/home/.local/share/supertuxkart/addons/karts`

Or copy individual karts to the add-ons directory, if not all of them are desired. Upon starting STK, you should see the loading screen fill up at least up to the loading icon. It may fill up past that point if other add-on karts are installed. This progress will not be seen if this collection is being installed for a STK installation starting in a server-only mode or no graphics mode.

## Tips
* Your opponents cannot be chosen (unless you're playing a local multiplayer game, or starting the game from the command-line)! They are decided at random, and depending on how many AI karts are set up (up to 20 by default), more than 8 fighters may be seen in one round.

* To race only with the SSBU fighters, select the 'Smash-Fighters' tab before selecting the kart. To do the same but with the alternate costumes, select the 'Smash-Alternates' tab instead. To race with all other groups of karts, select the 'All' tab before selecting the kart. Choosing this latter option basically enables racing with a much larger crossover, including the 'standard' group and 'Add-Ons' group if any karts from the official add-ons website are installed.

* Every character shares the same kart, and a logo for the fictional in-game company Mandarava Motors appears on the kart chassis. It's there to imply that that company is mass-manufacturing that kart, though my redo puts less emphasis on it.

## Other modes to try out in-game:
* Time trial - You can race not only by yourself, but also add in AI karts or other players for a better challenge.

* Follow the leader - Stay ahead of other karts, but don't overtake the leader. This mode does not use laps.

* Three strikes battle - Battle against other karts with powerups! This modes bears the most resemblence to the Smash games, but you drive around in a 3D track/arena, instead of fighting on 2D platforms.

* Soccer - Use your kart to push the soccer ball or other object into the opposite-colored goal, depending on if you picked red or blue. This kind of game has never been explored in any Smash game, so this is new for many of the characters.

* Easter egg hunt - Explore tracks to find blue easter eggs.

* Ghost replays - Race against ghosts to try beating them, or just watch what they did.

## Online play support
It is possible to use this kart collection online as of version 1.0.0, like with all other add-on karts. To get the best experience:

* A server and other players on it should have this collection already installed locally, so that everyone can see these karts.
* Should the server or a player not have them installed, they will be replaced by Tux (the mascot of the Linux kernel) for the players that don't have these karts, and the players using them will have their kart type changed to medium.

## Increasing the skinning joints limit
The karts are unoptimized in that most of them contain more than 50 skinning joints, most of which go unused. This will pose problems if racing with > 12 of these karts in a stock STK installation, as it is configured to permit only up to 1024 skinning joints by default. This limit can be increased to as high as 65535, in order to permit racing with more of these karts at a time, at the expense of reduced framerates and performance. To accomplish this:

* Navigate to the STK data directory. Usually this is where the game data files are installed.
* Find the file at `<location of data directory>/data/stk_config.xml`; this is where some of the additional game options can be configured, but which cannot be configured normally by the user. If this file is located in a read-only location, such as in a shared installation, it may not be possible to edit this file, so playing.with fewer of these karts is the only option.
* Find the lines reading

```xml
  <!-- Maximum bones from all animated meshes in each frame to be uploaded for
       hardware skinning, For gles 3.0 the specification guarantees at least 2048, for
       TBO in desktop at least 65536 (max buffer size) / 64, SSBO at least 2^24 / 64,
       so 1024 will work everywhere. -->
  <skinning max-bones="1024"/>
```
They are located near the end of the config file. Change `skinning max-bones` to a value higher than 1024. 65535 is a good limit for computers with decent GPUs, but a lower value may be preferred for computers with lower-end GPUs and mobile devices. 
