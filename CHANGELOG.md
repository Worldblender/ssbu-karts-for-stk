# Changelog
*Super Smash Bros. Ultimate will be referred to as Smash Ultimate here.*

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
but this project does not use [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
Instead, keywords with a suffixed number are used for the releases as this is more of an audiovisual project.

## [Alpha1] - 2021-01-28
### Post-release changes
* Remove accidental vertex coloring on the front kart lights for Steve's kart.
* Increase the width of the seat for Kirby's kart.

### Base Kart
* New base kart for all characters, with a design inspired by the Standard Karts found in Mario Kart 8 / Mario Kart 8 Deluxe. It is also slightly larger than the previous base kart used, but its height is lower compared to it.
* The new base kart is considerably higher in polygon count compared to the old base kart. Every kart takes up slightly more disk space, and there may be a trivial performance hit (less noticeable on computers with better GPUs, more noticeable on computers with lower-end GPUs and mobile devices).
* Only one headlight is used this time, instead of two from my previous attempt.
* Eight smaller lights are found at the back of the kart, and the side and back sections also have blue light areas that light up green in tracks that take place during the night. The front of the kart also has blue light areas, but they don't light up green at night.
* The Smash Bros. symbol is found at the back of every kart, and lights up in tracks that take place during the night.
* Actual exhaust pipes built into the engine, with the two nitro emitters being placed considerably higher than from my previous base kart.

### Roster
* Add all of the DLC characters released so far, being up-to-date with version 10.1.0 in Smash Ultimate.
* All character names are exactly as how they appear in Smash Ultimate, except for not being all uppercase. This effectively drops most prefixes and last names.

### Character appearance
* Larger characters that can be seen more easily, with the scaling increased to between 0.03 to 0.05 Blender units. 0.05 is the most common scaling factor used.
* Most characters with more realistic hair now use the `alphatest` shader instead of the `alphablend` shader for their hair and eyebrows. A lot less hair being lit in the dark! A side effect of this shader change is that such characters now look slightly more cartoony, also making them fit slightly better with the official STK style.
* Unique vertex color permutations for every character based on themselves. No more guessing who is in a generic kart!
* Larger characters may have the steering wheel pointing upwards instead of facing them.
* Better sitting positions for most characters.
* Make all characters that have hands actually hold the steering wheel.
* Certain characters that don't sit down like humans do have their seats removed entirely.

### Notable character-specific changes
* Duck Hunt - The duck stands on top of the engine, not on the dog's back.
* Mr. Game & Watch - Appears in his original black and gray glory, no longer appearing in purple. However, he still appears in 3D, and some of his animations may not have the choppy appearance.
* Olimar - The glass material for his helmet has been replaced by a texture shipping with STK. The winged Pikmin have been removed. Instead, 5 different Pikmin (red, yellow, blue, white, and purple) appear around the kart. Olimar is no longer alone while racing!
* Piranha Plant: The feet are no longer visible; it rests directly on its pot. Its neck is more naturally curved.
* R.O.B. - Drives by using its arms and not the steering wheel, like in its appearance in Mario Kart DS.
* Wario - The glass material for the goggles on his helmet has been replaced by a texture shipping with STK.
