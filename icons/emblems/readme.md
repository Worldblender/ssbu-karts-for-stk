## Series Symbols For Fighters
All of the SVG files found here can be obtained from https://www.ssbwiki.com/Series_symbol. Only the symbols representing fighters are included here; other symbols listed from the original source are not useful here.
