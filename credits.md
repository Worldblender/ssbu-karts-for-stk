## Credits
* Base kart - current version (standard-mk8) licensed under CC-BY-SA 4.0:
    * The new, Mario Kart 8 inspired, base kart is based on content from <https://blendswap.com/blend/12954>; originally created by Diro, but labelled as 'fan art'. This version is licensed under the Creative Commons Attribution 3.0 Unported (CC BY 3.0).
    * The old base kart is based on content from <https://opengameart.org/content/racing-kart> - my changes to it are not subject to non-commercial restrictions; original version by Keith at Fertile Soil Productions. This version is licensed under the Creative Commons Zero 1.0 Universal (CC0 1.0) Public Domain Dedication.

* From the TRaK2 Texture set By Georges "TRaK" Grondin: License - GPLv2+ or CC-BY-SA 3.0:
    * <https://opengameart.org/content/brown-metal-with-small-holes> -> trak2_holes1b.tga
    * <https://opengameart.org/content/metal-plate> -> trak2_metal2b.tga
    * <https://opengameart.org/content/old-blue-white-metal> -> trak2_metal1a.tga
    * <https://opengameart.org/content/old-blue-grey-metal> -> trak2_metal1b.tga
    * <https://opengameart.org/content/old-blue-black-metal> -> trak2_metal1c.tga

From SuperTuxKart itself:

* stkflag_mandaravaMotors_a.svg: CC BY-SA 4.0; mandaravaMotors_logo.png is derived from this texture, and is under this same license.

* Light cones - copied from Amanda's kart (old base kart) and Konqi's / Xue's kart (new base kart)

* 'stkkart_leatherShoes_a.png' - The leather texture copied from Sara the Wizard's kart

* Wheels - copied from Suzanne's kart, and texture replaced with in-game version

* Christmas hat and Easter ears: CC BY-SA 4.0

The icons for the Smash fighters can be found at and at <https://www.spriters-resource.com/nintendo_switch/supersmashbrosultimate/>, ripped by Random Talking Bush.

This repository tracks the model files' thread about them at <https://www.vg-resource.com/thread-34836.html>
