# SuperTuxKart × Super Smash Bros. Ultimate: Kart Collection For SuperTuxKart (STK)
*Everyone is here ... for kart racing! Except for the Mii Fighters and the Pokémon Trainer himself/herself.*

A collection of addon karts for use with free/open source (FOSS) racing game [SuperTuxKart](https://supertuxkart.net/) based on the roster of Super Smash Bros. Ultimate (SSBU). Every playable character from that game will be included in this set, excluding the Mii Fighters, as their customizability cannot be implemented in STK. The Pokémon Trainer himself/herself does not appear, but their Pokémon do appear as three separate karts.

All downloadable content (DLC) fighters are included in this collection. **A total of 86 karts is currently provided by this collection; no more new karts besides those of existing characters' alternate costumes that are not mere recolors will be added.**

**The new base kart now takes inspiration from the Standard Karts found in Mario Kart 8 / Mario Kart 8 Deluxe (but is not a ripped model). As there exist images of some of the fighters sitting in such similar karts, this may make it easier for me to pose some of the fighters.**

Each karts' directory is found under the original name that appears in the game data for SSBU, including any Japanese names used there. They are loaded in that order, but they use their international full names on the kart selection screen.

The karts are classified into two groups to prevent cluttering: 'Smash-Fighters' and 'Smash-Alternates'. 'Smash-Fighters' is for the default costume of all fighters, while 'Smash-Alternates' is for the alternate costumes of some fighters. Only the alt costumes that contain significant changes (different character, gender, or outfit) are found here. Almost no palette swaps are found here, with the only exception being the gray western version of R.O.B..

Both the kart files and the source Blender files are found here. This repository is self-contained, where everything that is needed is inside. The stk-media repo or stk-assets repositories are useful for working with the source files, but both are not required, which makes it possible to reuse the source files elsewhere. The source Blender files require Blender 2.80 or newer to be opened; older versions will not open them properly.

This repository uses Git Large File Storage (LFS), in order to keep the repository sizes more manageable. See https://git-lfs.github.com/ for more information and how to get the client.

Karts-only downloads: <https://drive.google.com/drive/folders/1Lu1aS4lUVr_MFaRcuUp3byLaolmFL8Hl?usp=sharing>

## Kart Previews
[Preview of all the karts exported](/screenshots/karts_default_all.png) 

Individual screenshots can be found in each source directory of every character.

## How to use/install
Can be found in [`install.md`](/install.md)

## Detailed Kart Information and changelog
Can be found in [`kart-information.html`](/kart-information.html)

The changelong can be found in [`CHANGELOG.md`](/CHANGELOG.md)

## Premise: How the SSBU Fighters got into the Mascot Kingdom
Can be found in [`premise.md`](/premise.md)

## Errata, Limitations, Todo
Can be found in [`errata.md`](/errata.md)

## Rationale
The purpose of this project is to satisfy three objectives at once (a.k.a. kill three birds with one stone):

* Satisfy some peoples' desires to see a crossover racing game featuring the SSB cast. The addon collection is also intended to provide an alternative to the fighting genre for those who are tired of fighting games, but still want a crossover of some sort.

* Practice 3D texturing and animation skills that can be reused in my future projects.

* Promote FOSS by showing people what STK is capable of doing, and that it isn't a mere ripoff of other kart racing games. *Disclosure: My real name appears in the credits for STK, where I am listed as a donor.*

## Current status of the package as a whole
**The addons are considered to be alpha status, and are being initially released as [minimum viable products](https://en.wikipedia.org/wiki/Minimum_viable_product).** That is, they work and have minimal animation (only steering animations that involve the characters turning their heads), but are not really polished like the karts that ship with STK. Don't expect things to be grandeur at the moment; I'm not fully experienced with 3D animation, but I'm getting there.
All of the fighters share a single kart linked to their source files to make it easier to manage them, but it caused a loss in showing off some uniqueness of each character. See </todo.md> for more information.

### Related Resources
* This project follows <https://gitlab.com/Worldblender/smash-ultimate-models-exported> for the status of characters. Head over there to use the characters for things other than kart racing.
* My Blender importer scripts at <https://gitlab.com/Worldblender/io_scene_numdlb/> have been a great help in my redo of all of the karts, and can be used to import the original Smash Ultimate model files.
* My port of the STK Blender exporter scripts at <https://github.com/RQWorldblender/stk-blender/> have allowed me to migrate my workflow to the newer Blender 2.80 and later versions.
* [Official STK addon website](https://online.supertuxkart.net/) (I strongly urge against uploading this kart collection to over there; they will likely be rejected due to incompatible licensing of the characters appearing in the karts)

### Credits
Can be found in </credits.md>

## Licensing
**Unless otherwise specified below, everything in './karts/', and './source/fighters/' is licensed under the CC-BY-NC 4.0 license.** Obviously, however, I do not claim any rights over the characters appearing in the karts; this is purely a fan creation. Now comes the specific items that are exempt from noncommercial restrictions:

* Original documentation - CC-BY-SA 4.0

* Original scripts - CC0

* The current base kart in './source/standard-mk8/*' - CC-BY-SA 4.0, originally created by Diro

* The objects './source/objects/christmas_hat.\*' and './source/objects/easter_ears.\*' - CC-BY-SA 4.0 and copied from the STK media repository

* The old base kart in './source/empty/*' (this also applies to copies of these files appearing in './karts/'):
  * kart-base*.*, genericshadow.png - CC-BY-SA 4.0
  * applications-other* - Tango Icon Project
  * stk*.png - CC-BY-SA 3.0 - SuperTuxKart textures
  * trak2_*.png - GPLv2+ or CC-BY-SA 3.0 - TRaK2 Texture set By Georges "TRaK" Grondin
  * bed.blend - CC0 - Created by Clint Bellanger, obtained from https://opengameart.org/content/bed-low-poly (only pillow is used)
  * chair.blend - CC-BY-SA 3.0 - https://opengameart.org/content/cinematheatre-chair

* In './source/fighters/pikmin/':
  * stk_glassPanel_a.png - CC-BY-SA 3.0 - SuperTuxKart textures
