*Super Smash Bros. Ultimate will be referred to as Smash Ultimate here.*
## Errata: Known limitations for the karts
* Only the default costumes of every fighter are here for now. Alternate costumes will be worked on later, with those that give different characters and those providing gender variants being given higher priority.

* All fighters are shrinked by between 0.03 to 0.05 Blender units in order to fit in the kart, with 0.05 being the most common scaling factor. This scaling is much better than from my previous attempt at 0.025, with the cartoony characters benefiting the most from the increased scaling (their eyes can actually be seen better). However, the eyes for some of the realistic characters are still quite hard to see.

* Limitations in the SPM (Space-Partitioned Mesh) format prevent identifying textures by material name, hence some characters require another copy of their eye white texture for both their left and right irises. Blender and the original Smash Ultimate are not bound by these limitations, allowing to eliminate duplicate images and saving video RAM in the process.

* Clipping (where a mesh intersects with another) has been minimized for every kart, except for the areas that cannot be normally seen in-game. However, other clipping is unavoidable especially for characters with any of the following while using a swatter:
  * Capes/mantles
  * Long hair
  * Long tails
  * Secondary characters sitting or standing on top on the engine

* When using kart colorization, some characters can end up getting affected unintentionally by it, especially if they use textures that have areas with an alpha channel, and even if such textures are not configured to display with transparency. The original Smash Ultimate reuses enabling and disabling the alpha channel on one texture likely in order to reduce video RAM usage. Stripping the alpha channel on such textures where it is not desired or disabling kart colorization are the only workarounds for this issue.

* For the Ice Climbers, STK warns about duplicated bones because it can cause kart attachments (the Christmas hat and Easter ears) to not work properly. This warning is not seen in-game, but in the logs or a terminal window. Every bone in Popo's and/or Nana's armature has to be renamed for this issue to be resolved.

* The high-polygon shapes for some characters have not been exported, in an effort to speed up export times and to increase performance. Such characters may look slightly worse as a result, but this really won't matter, since they are seen up close less often.

* The karts are unoptimized in that most of them contain more than 50 skinning joints, most of which go unused. This will pose problems if racing with > 12 of these karts in a stock STK installation, as it is configured to permit only up to 1024 skinning joints by default. This limit can be increased to as high as 65535, in order to permit racing with more of these karts at a time, at the expense of reduced framerates and performance.

* The characters are not as dynamic or expressive as they are in the original Smash Ultimate. It is possible to extend STK to support per-kart sound effects to support part of this effort. Only skeletal animation and not interpolation animation is supported, and every character makes use of toggling the visibility of certain meshes to change most of their expressions. While this setup can be done in Blender with multiple actions, this is more challenging to accomplish with only skeletal animation.

* Some characters have pale skin where they should be more natural-looking. In Smash Ultimate, additional shaders are used to make such characters' skin look better. As such shaders are likely proprietary and won't be implemented in STK anytime soon, the only workaround for this is to alter the skin textures to make them look better.

## Out of scope: What this project will not do
* Only playable fighter characters are within this collection's scope. Bosses and assist trophies are unlikely to be included at all, since they generally are not playable characters.

* Super Mario characters are part of this collection, but only those that appear in SSBU as fighters, so it also does part Mario Kart. The non-Mario characters outnumber the Mario characters though, so it's best to call it like Smash Bros. Kart.

* Tracks are not provided by this collection, so the characters will end up racing in an original world created for STK, the [Mascot Kingdom](https://supertuxkart.net/Style), and whatever else appears in its addon site. This situation is somewhat similar to Super Smash Bros. Brawl's Subspace Emissary, where fighters appear in a mostly generic world with generic enemies. However, stay tuned for another project that will port some of the Smash Ultimate stages as STK tracks!

* This is not a STK -> Smash Bros. complete transformation package; you'll still be playing STK as this addon collection only provides karts. Unless STK is forked or rebranded, it is not possible to completely remove the presence of Tux, Thunderbird (the namesake mascot of the Mozilla e-mail client acting as the game's referee), GNU, Nolok, and some other karts shipping with STK.

## Wishlist / Todo: Things that can be improved
* Replace the front Mandarava Motors logo with the symbols of the universes each character originates from.

* Tweak the sizes of some characters to be closer to canon, though not too small that they can't be easily seen with another kart.

* Rename the bones in the Ice Climbers' kart to not conflict with each other.

* Use normal maps and PRM maps to make more materials look better in-game.

* Unique karts for most characters that better highlight their personality or home universes.

* More animations, such as winning and losing ones. In an effort to get every fighter out the door more quickly, they contain very minimal animation this time. The only animation implemented is for the straight frame. To make the characters more dynamic, here is a list of markers that can be implemented:
  * Steering - left, right
  * Winning and Losing - start-winning, start-winning-loop, end-winning, start-losing, start-losing-loop, end-losing
  * Jump - start-jump, start-end
  * Kart Selection Screen - selection-start, selection-end
  * Backpedal (Looking backwards on reversing the kart) - backpedal-left, backpedal, backpedal-right

* The hairbands, mantles, capes, scarves, bandanas, skirts, aprons, long hair,  wings, and partner characters for some fighters can be implemented as speed-weighted objects. The following characters are candidates for getting a speed-weighted object:
  * Bayonetta
  * Captain Falcon
  * Chrom
  * Corrin
  * Dark Pit
  * Daisy
  * Donkey Kong
  * Dr. Mario
  * Ganondorf
  * Greninja
  * Ike
  * Ken
  * King Dedede
  * King K. Rool
  * Lucina
  * Marth
  * Meta Knight
  * Olimar
  * Palutena
  * Peach
  * Pit
  * Richter
  * Robin
  * Rosalina & Luma
  * Roy
  * Ryu
  * Sephiroth
  * Sheik
  * Simon
  * Snake
  * Zelda
